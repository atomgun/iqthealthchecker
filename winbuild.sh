#!/bin/sh
CUR=$(pwd)
cd ./src
env GOOS=windows GOARCH=amd64 GOARM=5 go build -ldflags="-s -w" -o $CUR/iqhealthchecker.exe
