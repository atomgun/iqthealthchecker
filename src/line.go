package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func sendLineNotify(token, message string) error {

	apiURL := "https://notify-api.line.me"
	resource := "/api/notify"
	data := url.Values{}
	data.Set("message", message)

	u, _ := url.ParseRequestURI(apiURL)
	u.Path = resource
	urlStr := u.String() // "https://notify-api.line.me/api/notify"

	client := &http.Client{}
	r, _ := http.NewRequest("POST", urlStr, strings.NewReader(data.Encode())) // URL-encoded payload
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	resp, _ := client.Do(r)
	defer resp.Body.Close() // MUST CLOSED THIS, https://hackernoon.com/avoiding-memory-leak-in-golang-api-1843ef45fca8

	body, _ := ioutil.ReadAll(resp.Body)
	dogLogger.Infof("Line Notify RESP : %s\r\n", body)
	resultStr := ""
	switch resp.StatusCode {
	case 200:
		resultStr = "Line Notify SUCCESS"
	default:
		resultStr = "Line Notify  ERROR"
	}
	dogLogger.Infof("Line Notify HTTP Status : %v %v\r\n\n", resp.StatusCode, resultStr)
	return nil
}
