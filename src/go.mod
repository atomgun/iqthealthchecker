module bitbucket.org/atomgun/iqhealthchecker

go 1.12

require (
	bitbucket.org/atomgun/iqlogger v0.0.0-20190530172714-2f802a931429
	github.com/fsnotify/fsnotify v1.4.7
	github.com/spf13/viper v1.4.0
)
