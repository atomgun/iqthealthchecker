package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/atomgun/iqlogger"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

// WatchApp ...
type WatchApp struct {
	name           string
	healthCheckURL string
	httpTimeout    time.Duration
	interval       time.Duration
	retryCount     int
	action         string
}
type appConfig struct {
	watchApps       []WatchApp
	lineNotifyToken []string
}

var (
	flagConfigFile = flag.String("config", "config.yaml", "config file")
	flagHelp       = flag.Bool("h", false, "Help")
	config         appConfig
)

func init() {
	flag.Parse()
	if *flagHelp == true {
		flag.Usage()
		os.Exit(0)
	}

	// Load config
	secretFileName := "secret"
	configFileName := "config"
	configFileType := "yaml"
	if *flagConfigFile == "" {
		// use default
	} else {
		configFileName = strings.TrimSuffix(filepath.Base(*flagConfigFile), filepath.Ext(*flagConfigFile))
		configFileType = strings.TrimPrefix(filepath.Ext(*flagConfigFile), ".")
	}

	viper.SetConfigName(configFileName) // name of config file (without extension)
	viper.SetConfigType(configFileType) // or viper.SetConfigType("YAML")
	viper.AddConfigPath("./config")     // path to look for the config file in
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s \n", err)
	}

	app := WatchApp{}
	app.name = viper.GetString("apps.name")
	app.healthCheckURL = viper.GetString("apps.healthCheckURL")
	app.httpTimeout = viper.GetDuration("apps.httpTimeout") * time.Second
	app.interval = viper.GetDuration("apps.interval") * time.Second
	app.retryCount = viper.GetInt("apps.retryCount")
	app.action = viper.GetString("apps.action")
	config.watchApps = append(config.watchApps, app)

	viper.SetConfigName(secretFileName) // name of config file (without extension)
	viper.SetConfigType(configFileType) // or viper.SetConfigType("YAML")
	viper.AddConfigPath("./secret")     // path to look for the config file in
	err = viper.ReadInConfig()          // Find and read the config file
	if err != nil {                     // Handle errors reading the config file
		log.Fatalf("Fatal error config file: %s \n", err)
	}
	config.lineNotifyToken = append(config.lineNotifyToken, viper.GetStringSlice("lineNotifyToken")...)
	log.Printf("Load config : %v\r\n", config)

	// Validate config
	// if config.SFTP.Host == "" {
	// 	log.Println("CONFIG ERROR : SFTP Config invalid")
	// 	os.Exit(1)
	// }

	err = initLogger()
	if err != nil {
		log.Printf("LOGGER ERROR : %s\r\n", err)
		os.Exit(1)
	}
}
func initLogger() error {
	log.Println("Init Logger")
	err := os.MkdirAll("./log", os.ModePerm)
	if err != nil {
		log.Printf("MkdirAll %s\r\n", err)
		return err
	}
	// Init(logLevel, logFormat uint32, filenamePrefix string, withDateSuffix, toStdout bool, logfileCloseTimeout int)
	dogLogger.Init(iqlogger.LoglvlDebug, iqlogger.LogFmtText, "iqwatchdog", true, true, 10)
	return nil
}

// RunCMD is a simple wrapper around terminal commands
func RunCMD(path string, args []string, debug bool) (out string, err error) {
	cmd := exec.Command(path, args...)
	var b []byte
	b, err = cmd.CombinedOutput()
	out = string(b)

	if debug {
		fmt.Println("DEBUG : ", strings.Join(cmd.Args[:], " "))

		if err != nil {
			fmt.Println("RunCMD ERROR")
			fmt.Println(out)
		}
	}

	return
}

var checkErrorCount = 0

// var lastActionTime := time.Time
func (app *WatchApp) checkerStart() {
	log.Println("Check ", app.name)
	time.AfterFunc(app.interval, func() {
		app.checkerStart()
	})
	err := HTTPGetCheck(app.healthCheckURL, app.httpTimeout)
	if err != nil {
		checkErrorCount++
		dogLogger.Errorf(err.Error())
		dogLogger.Errorf("%s DOWN !!!, [%d]", app.name, checkErrorCount)
		if checkErrorCount >= app.retryCount && checkErrorCount%app.retryCount == 0 {
			cmd := exec.Command("bash", "-c", app.action)
			var b []byte
			b, _ = cmd.CombinedOutput()
			out := string(b)
			dogLogger.Infof("Action result : %s", out)

			lineMessage := fmt.Sprintf("%s DOWN !!!, [%d] take action\r\nResult : %s", app.name, checkErrorCount, out)
			sendLineNotify(config.lineNotifyToken[0], lineMessage)
		}
	} else {
		dogLogger.Infof("%s UP", app.name)
		if checkErrorCount > 0 {
			lineMessage := fmt.Sprintf("%s Recovery Success !!!", app.name)
			sendLineNotify(config.lineNotifyToken[0], lineMessage)
			checkErrorCount = 0
		}
	}
}
func main() {
	for _, app := range config.watchApps {
		app.checkerStart()
	}

	for {
		time.Sleep(20 * time.Second)
	}
}
